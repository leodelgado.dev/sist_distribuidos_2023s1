# Detector


- ¿Qué mensaje se da como razón cuando el nodo que estamos monitoreando es terminado? 

```
{producer,'silver@imendez-MS-7A38'} died; {badarith,[{producer,producer,3,[{file,[112,114,111,100,117,99,101,114,46,101,114,108]},{line,27}]}]}
```

-  ¿Qué sucede si matamos el nodo Erlang en el producer? 

```
{producer,'silver@imendez-MS-7A38'} died; noconnection
```

- ¿Qué significa haber recibido un mensaje ’DOWN’? ¿Cuándo debemos
confiar en el?

El mensaje 'DOWN' es el mensaje que llega del monitor cuando el proceso que estamos monitoreando termina, es confiable para determinar esto mismo pero no es confiable de que haya sido por un error, podría haber terminado voluntariamente.


- ¿Se recibieron mensajes fuera de orden, aun sin haber recibido un
mensaje ’DOWN’? ¿Qué dice el manual acerca de las garantías de envíos
de mensajes?

Segun el manual Erlang usa una queue de mensajes para el receive de cada proceso que identifica todos los mensajes por orden de llegada, sin posibilidad de que cambien de orden accidentalmente. Sin embargo cuando hicimos pruebas en los que el nodo del producer pierde la conexión y ocurre un timeout, se pierden mensajes y el próximo mensaje recibido no es el esperado si eventualmente se recupera la conexión.

El nodo del producer luego del timeout descarta todos los mensajes que envíe hasta que vuelva a recuperar la conexión.

- Ahora probemos desconectar el cable de red de la máquina corriendo el
producer y volvamos a enchufarlo despues de unos segundos. ¿Qué pasa?
Desconectemos el cable por períodos mas largos. ¿Qué pasa ahora?

El consumer sigue esperando mensajes del producer pero este mismo dejó de recibirlos. En el momento en que volvemos a enchufar el cable de red, recibirá todos los mensajes juntos enviados en el periodo en el que estuvo desconectado. Esto va a ocurrir siempre y cuando no haya un timeout configurado en el consumer o no se supere el timeout por defecto de Erlang (aproximadamente 1 minuto).

En el caso de que ocurra un timeout pero los procesos sigan en ejecucion, si vuelve la conexion ocurrira un desfasaje de un unico mensaje esperado con mensaje recibido. Luego de eso sigue recibiendo mensajes y ejecutando normalmente.