%%-------------------------------------------------------------------
%% @doc detector public API
%% @end
%%%-------------------------------------------------------------------

-module(detector_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
    detector_sup:start_link().

stop(_State) ->
    ok.

%% internal functions
