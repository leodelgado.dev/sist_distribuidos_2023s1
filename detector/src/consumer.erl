-module(consumer).
-export([start/1, stop/0]).

start(Producer) ->
    C = spawn(fun() -> Monitor = monitor(process, Producer), consumer(0, Monitor) end),
    Producer ! {hello, C},
    register(consumer, C).

stop() ->
    consumer ! bye. 

consumer(ExpectedValue, Monitor) ->
    receive
        {ping, N} ->
            if 
                N == ExpectedValue -> io:format("Got ping ~p~n", [N]);
                true -> io:format("Expected ~p, got ~p, eye~n", [ExpectedValue, N])
            end,
            consumer(N+1, Monitor);
        bye ->
            io:format("k bye~n");
    {'DOWN', Monitor, process, Object, Info} ->
        io:format("~w died; ~w~n", [Object, Info]),
        consumer(ExpectedValue, Monitor)
    end.
