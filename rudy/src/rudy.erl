-module(rudy).
-export([init/1, handler/2, reply/1, request/1]).

init(Port) ->
    Opt = [list, {active, false}, {reuseaddr, true}],
    case gen_tcp:listen(Port, Opt) of
        {ok, Listen} ->
            R = spawn(fun() -> handler(Listen, true) end),
            register(handler, R), 
            receive 
                kill -> 
                    io:format("[Rudy] muriendo :(~n"),
                    handler ! kill,
                    kill
            end,
            gen_tcp:close(Listen),
            ok;
        {error, Error} ->
            Error
    end.

handler(Listen, KeepAlive) ->
    case gen_tcp:accept(Listen) of
        {ok, Client} ->
            spawn(fun() -> request(Client) end),
            receive 
                kill ->
                    io:format("[Handler] muriendo :(~n"),
                    handler(Listen, false)
            after 
                0 ->
                    if KeepAlive -> handler(Listen, KeepAlive);
                    true -> ok
                    end
            end;
        {error, Error} ->
            io:format("[Handler] error: ~w~n", [Error]),
            case Error of 
                closed -> ok;
                _ -> handler(Listen, KeepAlive)
            end
    end.

request(Client) ->
    Recv = gen_tcp:recv(Client, 0),
    case Recv of
        {ok, Str} ->
            Request = http:parse_request(Str),
            Response = reply(Request),
            gen_tcp:send(Client, Response);
        {error, Error} ->
            io:format("[Request] error: ~w~n", [Error])
    end,
    gen_tcp:close(Client).


reply({{get, URI, _}, _, _}) ->
    timer:sleep(40),
    case URI of
        "/api/hola" ->
            http:ok("Hola :)");
        "/api/chau" ->
            http:ok("Chau chau!");
        "/api/index.html" ->
            find_file("/index.html");
        _ ->
            http:ok("[NOT_FOUND] :(")
    end.


find_file(URI) ->
    case file:read_file("./src/public" ++ URI) of
        {ok, Binary} -> http:ok(Binary);
        {error, enoent} -> http:ok("[NOT_FOUND] no se pudo encontrar el archivo");
        {error, _} -> http:ok("[INTERNAL_ERROR] Ocurrio un error")
    end.
