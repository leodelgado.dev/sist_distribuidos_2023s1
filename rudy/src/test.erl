-module(test).
-export([bench/3, bench/0]).

bench() ->
    bench("localhost", 8080, 100).

bench(Host, Port, NumberOfRequests) ->
    COUNTER = spawn(fun() -> benchRequestCounter(0, erlang:system_time(milli_seconds), NumberOfRequests) end),
    run(NumberOfRequests, Host, Port, COUNTER).


benchRequestCounter(Counter, Starttime, N) ->
    receive
        _ ->
            NewCounter = Counter + 1,
            case NewCounter >= N of
                true ->
                    Finish = erlang:system_time(milli_seconds),
                    io:format("Performed ~w operations in ~p milliseconds~n", [N, Finish - Starttime]);
                false ->
                    benchRequestCounter(NewCounter, Starttime, N)
        end
    end.

run(N, Host, Port, BenchRequestCounter) ->
    if
        N == 0 ->
            ok;
        true ->
            spawn(fun() -> request(Host, Port, BenchRequestCounter) end),
            run(N-1, Host, Port, BenchRequestCounter)
    end.

request(Host, Port, BenchRequestCounter) ->
    Opt = [list, {active, false}, {reuseaddr, true}],
    {ok, Server} = gen_tcp:connect(Host, Port, Opt),
    gen_tcp:send(Server, http:get("api/hola")),
    Recv = gen_tcp:recv(Server, 0),
    case Recv of
        {ok, _} ->
            BenchRequestCounter ! message;
        {error, Error} ->
            io:format("test: error: ~w~n", [Error])
    end,
    gen_tcp:close(Server).
