-module(server).
-export([start/1, start/0, stop/0]).

start(Port) ->
    register(rudy, spawn(fun() -> rudy:init(Port) end)).

start() ->
    start(8080).

stop() ->
    rudy ! kill.
