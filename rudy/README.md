# Rudy
=====

## Benchmark
- El benchmark está hecho en Erlang de tal forma que opere de manera concurrente con respecto a las peticiones que reciba.
- Hicimos varias mediciones con varias cantidades de requests y calculamos el promedio para todos los valores.

###  ¿cuántos requests por segundo podemos servir? 
- aproximandamente 30 requests por segundo.

### ¿Nuestro delay artificial es importante o desaparece dentro del overhead de parsing?
- Es importante para cantidades pequeñas de requests pero empieza a desaparecer rápidamente al pasar las 20 requests del benchmark, con lo cual es mayormente insignificante comparado con el overhead del parsing.

### ¿Qué ocurre si ejecutamos los benchmarks en varias máquinas al mismo tiempo?
- En promedio cada benchmark es un 20% menos eficiente al estar siendo ejecutados al mismo tiempo.


### ¿Deberíamos crear un nuevo proceso por cada request de entrada?
- Depende, ya que si bien en nuestro ejercicio las requests son bastante simples, en otros sistemas donde cada request sea mucho más pesada y requiera más espacio en memoria por cada una, tener un proceso por cada una puede llegar a ser bastante pesado (además de que no todos los lenguajes de programación tienen la capacidad para crear miles de procesos con facilidad).

### ¿Toma tiempo crear un nuevo proceso? ¿Qué ocurriría si tenemos miles de requests por minuto?
- Erlang es un lenguaje pensado con la capacidad de atender miles de requests por minuto con facilidad, si pensamos que cada request va a ser un proceso.

### ¿Cómo sabremos el tamaño del cuerpo de una request que llega dividida en varios bloques?
- Disponemos del header `Content-Length` que nos indica si la request que está llegando está fraccionada y que debemos esperar a que se complete toda la información para procesarla.