# Loggy
___

### El módulo Time

El módulo Time nos permite tener una abstracción de la medición del tiempo transcurrido en el sistema distribuido, además de tener un protocolo externo a todos los procesos con el cual representar el tiempo transcurrido y el tiempo a ser loggeado y quitarle la responsabilidad de representar el tiempo a otros elementos del sistema que no les corresponde.

Al haberlo implementado de esta manera, nos permite cambiar la definición de lo que significa un tiempo; siempre y cuando respetemos el protocolo de la librería, podemos implementarlo de cualquier manera. En este proyecto decidimos implementarlo simplemente como numeros enteros pero bien podria haber sido una estructura más compleja.

## La primera implementación que hicimos usando el modulo Time nos genero entradas fuera de orden. Como las detectamos?

Hay dos condiciones por las que deben ser ordenados los logs:
- Ordenados segun tiempo lamport, donde el numero del tiempo que sale en el log debe siempre ser mayor o igual al del anterior log
- Para cada log de `received` debe haber ocurrido un log de `sending` correspondiente, ya que no tiene sentido que haya ocurrido una recepcion de un mensaje que no fue previamente enviado
Nuestra primer implementacion registro entradas fuera de orden y la detectamos porque no se cumplia la segunda condicion.

## Que es lo que el log final nos muestra?

Nos muestra el tiempo total en tiempo lamport que ocurrió desde que inició el test (que generalmente inicia en 0 o 1) hasta que finaliza (un numero N). Qué tan grande es este N depende de cuánto jitter y sleep exista (cuanto menos de ambos, más tiempo lamport transcurrirá).

## Los eventos ocurrieron en el mismo orden en que son presentados en el log?

Si bien la mayoria de los eventos llegan en orden, la realidad es que siempre que implementemos tiempos lamport nunca llegara la totalidad de los logs en orden.

## Que tan larga puede llegar a ser la cola de retencion del logger?

La cola de retención puede ser en el peor de los casos de tamaño N, donde N es la cantidad de workers - 1. Esto ocurre porque la cola retendrá a lo sumo todos los mensajes que lleguen fuera de tiempo a comparación de un log que debería ser anterior y que en el momento que llegue habilite al resto de mensajes en la cola a ser impresos (en el peor de los casos ocurrira que un mensaje de cada worker esté fuera de tiempo excepto de uno, el cual cuando llega habilita al resto de eventos a ser impresos).

