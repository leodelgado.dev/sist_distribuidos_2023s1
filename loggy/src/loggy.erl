-module(loggy).
-export([start/1, stop/1]).

start(Nodes) ->
    spawn_link(fun() ->init(Nodes) end).

stop(Logger) ->
    Logger ! stop.

init(Nodes) ->
    loop(time:clock(Nodes), []).

loop(Clock, Queue) ->
    receive
        {log, From, Time, Msg} ->
                C = time:update(From, Time, Clock),
                MsgQueue = [{From, Time, Msg}|Queue],
                {Safe, Unsafe} = lists:partition(fun({F, T, M}) -> time:safe(T, C) end, MsgQueue),
                lists:foreach(fun({F, T, M}) -> log(F, T, M) end, Safe),
                loop(C, Unsafe);
            stop ->
            ok
    end.

log(From, Time, Msg) ->
    io:format("log: ~w ~w ~p~n", [Time, From, Msg]).
    