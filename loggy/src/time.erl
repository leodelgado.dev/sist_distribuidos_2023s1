-module(time).
-export([zero/0, inc/2, merge/2, leq/2, clock/1, update/3, safe/2]).

zero() -> 
    0.

inc(Name, T) ->
    T + 1.

merge(Ti, Tj) ->
    max(Ti, Tj).

leq(Ti, Tj) ->
    Ti =< Tj.

clock(Nodes) ->
    lists:map(fun(Nodo) -> {Nodo, zero()} end, Nodes).


update(Node, Time, Clock) ->
    lists:map(
        fun({N, T}) -> 
            if N == Node -> {N, merge(Time, T)}; 
            true -> {N, T} 
            end 
        end, 
    Clock).

safe(Time, Clock) ->
    lists:all(fun({_, T}) -> leq(Time, T) end, Clock).
    