# Repositorio Sistemas Distribuidos 1er Cuatrimestre 2023
## Ignacio Mendez y Leonel Delgado

### Reportes de entregas
- Rudy: https://gitlab.com/leodelgado.dev/sist_distribuidos_2023s1/-/blob/main/rudy/README.md
- Detector: https://gitlab.com/leodelgado.dev/sist_distribuidos_2023s1/-/blob/main/detector/README.md
- Opty: https://gitlab.com/leodelgado.dev/sist_distribuidos_2023s1/-/blob/main/opty/README.md
- Loggy: https://gitlab.com/leodelgado.dev/sist_distribuidos_2023s1/-/blob/main/loggy/README.md
- Muty: https://gitlab.com/leodelgado.dev/sist_distribuidos_2023s1/-/blob/main/muty/README.md
- Toty: https://gitlab.com/leodelgado.dev/sist_distribuidos_2023s1/-/blob/main/toty/README.md
- Groupy: https://gitlab.com/leodelgado.dev/sist_distribuidos_2023s1/-/blob/main/groupy/README.md