-module(entry).
-export([new/0, lookup/2, add/3, remove/2]).

new() ->
    [].

lookup(Req, Entries) ->
    case lists:keysearch(Req, 1, Entries) of
        {value, {Req, Reply}} ->
            Reply;
        false ->
            unknown
    end.

add(Name, Entry, Entries) ->
    [{Name, Entry}|Entries].

remove(Name, Entries) -> 
    lists:keydelete(Name, 1, Entries).