-module(cache).
-export([new/0, lookup/2, add/4, remove/2]).

new() ->
    [].

lookup(Name, Cache) ->
    case lists:keysearch(Name, 1, Cache) of
        {value, {Name, Ttl, Reply}} ->
            Valid = time:valid(Ttl, time:now()), 
            if
                Valid ->
                    {ok, Reply};
                true ->
                    invalid
            end;
        false ->
            unknown
    end.

add(Domain, Ttl, Reply, Cache) ->
    [{Domain, Ttl, Reply}|Cache].

remove(Name, Cache) -> 
    lists:keydelete(Name, 1, Cache).
