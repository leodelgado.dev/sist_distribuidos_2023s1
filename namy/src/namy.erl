-module(namy).
-export([start/0, stop/0]).

start() ->
    %servers
    server:start(root, 10),
    server:start(net, net, root, 10),
    server:start(ar, ar, root, 10),
    server:start(com, com, ar, 10),
    server:start(edu, edu, ar, 10),
    server:start(unq, unq, edu, 10),

    %hosts
    host:start(nic, nic, ar),
    host:start(taringa, taringa, net),
    host:start(mercadolibre, mercadolibre, com),
    host:start(uba, uba, edu),
    host:start(mi, mi, unq),
    
    %resolvers
    resolver:start(root),

    %clients
    client:test([nic, ar], resolver).
    
stop() ->
    server:stop(),
    resolver:stop(),
    host:stop(pepito).
