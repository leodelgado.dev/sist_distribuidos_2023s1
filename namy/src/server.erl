-module(server).
-export([start/2, start/4, stop/1, init/1, init/3]).

start(ProcessName, TTL) ->
    register(ProcessName, spawn(server, init, [TTL])).

start(ProcessName, Domain, DNS, TTL) ->
    register(ProcessName, spawn(server, init, [Domain, DNS, TTL])).

stop(ProcessName) ->
    ProcessName ! stop,
    unregister(ProcessName).

init(TTL) ->
    server(entry:new(), TTL).

init(Domain, Parent, TTL) ->
    io:format("Register ~w with parent ~w ~n", [Domain, Parent]),
    Parent ! {register, Domain, {dns, self()}},
    server(entry:new(), TTL).

server(Entries, TTL) ->
    receive
        {request, From, Req}->
            io:format("request ~w~n", [Req]),
            Reply = entry:lookup(Req, Entries),
            From ! {reply, Reply, TTL},
            server(Entries, TTL);
        {register, Name, Entry} ->
            io:format("register ~w~n", [Name]),
            Updated = entry:add(Name, Entry, Entries),
            server(Updated, TTL);
        {deregister, Name} ->
            io:format("deregister ~w~n", [Name]),
            Updated = entry:remove(Name, Entries),
            server(Updated, TTL);
        {ttl, Sec} ->
            server(Entries, Sec);
        status ->
            io:format("cache ~w~n", [Entries]),
            server(Entries, TTL);
        stop ->
            io:format("closing down~n", []),
            ok;
        Error ->
            io:format("strange message ~w~n", [Error]),
            server(Entries, TTL)
    end.
