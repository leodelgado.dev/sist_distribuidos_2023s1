%%%-------------------------------------------------------------------
%% @doc namy public API
%% @end
%%%-------------------------------------------------------------------

-module(namy_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
    namy_sup:start_link().

stop(_State) ->
    ok.

%% internal functions
