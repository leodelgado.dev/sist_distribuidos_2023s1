# toty
=====

## Multicast Basico

### Que pasa si tenemos un sistema que se basa en una entrega con orden total pero esto no fue claramente establecido?

- En situaciones en las que el tiempo de sleep y jitter sean similares o casos en los que el jitter sea mayor al sleep el sistema sigue funcionando y los mensajes siguen llegando en orden.
- En situaciones en las que el tiempo de sleep sea mayor al jitter los mensajes empiezan a llegar completamente fuera de orden y empeorando en funcion del tiempo transcurrido y la diferencia de tiempos.

### Si la congestión es baja y no tenemos retrasos en la red, cuánto tiempo tarda antes de que los mensajes se entreguen fuera de orden?

- Asumiendo que congestion y retrasos en la red en nuestro sistema son representados por el tiempo de Jitter, el tiempo de cuanto demora en quedar fuera de orden depende de cuanta diferencia tenga con el tiempo de Sleep (a mayor diferencia, más pronto llegarán fuera de orden).

### Cuán difícil es hacer debug del sistema y darnos cuenta que es lo que está mal?

- Gracias a la GUI propuesta y a poner distintos logs en la recepcion y envio de mensajes del worker es relativamente sencillo hacer debug del sistema para saber si está funcionando bien o mal, sin embargo por la complejidad del mismo en caso de que funcione mal es difícil saber por qué.

## Multicast con Orden Total

### Mantiene los workers sincronizados? Tenemos muchos mensajes en el sistema? cuántos mensajes podemos hacer multicast por segundo y cómo depende esto del número de workers?

- Mantiene los workers sincronizados en cualquier situacion de tiempo de Sleep y Jitter en el sentido de mantener el orden de los mensajes, sin embargo no los mantiene sincronizados en el sentido de que todos tengan el mismo color en la GUI o a todos les llega el mismo mensaje al mismo tiempo ya que esto va a depender de los tiempos de congestión y retrasos (cuanto mayor sean, más probable que en la GUI se muestren con distintos colores por ejemplo).
- Hay muchos mensajes en el sistema, en principio a menor sleep y jitter más mensajes podremos enviar, sin embargo cuanto mayor sea el número de workers también es mayor el número de mensajes a sincronizar y más costoso se hace mantener el sistema, por lo que llegará un punto en el que tener más workers empiece a ser contraproducente.