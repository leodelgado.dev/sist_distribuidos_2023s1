-module(multicast1).
-export([start/1]).


start(Jitter) ->
    spawn(fun() -> init(Jitter) end).

init(Jitter) ->
    receive
        {peers, Peers} ->
            register_worker(Peers, Jitter);
        stop ->
            ok
    end.

register_worker(Peers, Jitter) ->
    receive
        {join, Worker} ->
            multicast(Peers, Jitter, Worker)
    end.

multicast(Peers, Jitter, Worker) ->
    receive
        {multicast, Ref, N} ->
            lists:map(fun(M) -> M ! {deliver, Ref, N} end, Peers),
            multicast(Peers, Jitter, Worker);
        {deliver, Ref, N} ->
            Worker ! {deliver, Ref, N},
            waiting(Peers, Jitter, Worker)
    end.
            

waiting(Peers, Jitter, Worker) ->
    Wait = random:uniform(Jitter),
    receive 
        after Wait ->
            multicast(Peers, Jitter, Worker)
    end.
