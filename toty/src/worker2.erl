-module(worker2).
-export([start/4]).

start(Name, Seed, Sleep, Multicast) ->
    spawn(fun() -> init(Name, Seed, Sleep, Multicast) end).

init(Name, Seed, Sleep, Multicast) ->
    Gui = spawn(gui, init, [Name]),
    random:seed(Seed, Seed, Seed),
    Multicast ! {join, self()},
    worker(Name, Sleep, Gui, Multicast, {0, 0, 0}),
    Gui ! stop.


% cada worker tiene su GUI, si esta funcionando bien todas las ventanitas
% deberian ser del mismo color
worker(Name, Sleep, Gui, Multicast, State) ->
    Wait = random:uniform(Sleep),
    receive
        stop ->
            ok
        after Wait ->
            send_multicast(Name, Sleep, Gui, Multicast, State)
    end.

update(State, N) ->
    {R, G, B} = State,
    {G, B, R+N rem 256}.

send_multicast(Name, Sleep, Gui, Multicast, State) ->
    Msg = random:uniform(20),
    Multicast ! {send, Msg},
    waiting(Name, Sleep, Gui, Multicast, State, Msg).

waiting(Name, Sleep, Gui, Multicast, State, Msg) ->
    receive
        stop ->
            ok;
        {deliver, _, N, {M, _}} when N == Msg ->
            io:format("[~s]Me llego el mio: ~w~n", [Name,M]),
            NewState = update(State, N),
            Gui ! {update, NewState},
            worker(Name, Sleep, Gui, Multicast, NewState);
        {deliver, _, N, {M, _}} ->
            io:format("[~s]Me llego otro: ~w~n", [Name,M]),
            NewState = update(State, N),
            Gui ! {update, NewState},
            waiting(Name, Sleep, Gui, Multicast, NewState, Msg)
    end.
