-module(multicast2).
-export([start/1]).

start(Jitter) ->
    spawn(fun() -> init(Jitter) end).

init(Jitter) ->
    receive
        {peers, Peers} ->
            register_worker(Peers, Jitter);
        stop ->
            ok
    end.

register_worker(Peers, Jitter) ->
    receive
        {join, Worker} ->
            multicast(Peers, Jitter, Worker, {0, self()}, [], [])
    end.

multicast(Nodes, Jitter, Master, Next, Cast, Queue) ->
    receive
        {request, From, Ref, Msg} ->
            From ! {proposal, Ref, Next},
            Queue2 = insert(Queue, Ref, Msg, Next),
            Next2 = increment(Next),
            multicast(Nodes, Jitter, Master, Next2, Cast, Queue2);
        {send, Msg} ->
            Ref = make_ref(),
            request(Nodes, self(), Ref, Msg),
            Cast2 = cast(Cast, length(Nodes), Ref),
            multicast(Nodes, Jitter, Master, Next, Cast2, Queue);
        {proposal, Ref, Proposal} ->
            case proposal(Proposal, Cast, Ref) of
                {agreed, Seq, Cast2} ->
                    agree(Nodes, Ref, Seq),
                    multicast(Nodes, Jitter, Master, Next, Cast2, Queue);
                Cast2 ->
                    multicast(Nodes, Jitter, Master, Next, Cast2, Queue)
            end;
        {agreed, Ref, Seq} ->
            Updated = update(Queue, Ref, Seq),
            {Agreed, Queue2} = agreed(Seq, Updated),
            deliver(Agreed, Master, Jitter),
            Next2 = increment(Next, Seq),
            multicast(Nodes, Jitter, Master, Next2, Cast, Queue2)
    end.

deliver(Agreed, Master, Jitter) ->
    lists:map(fun({_, Ref, Msg, Seq }) -> 
        Master ! {deliver, Ref, Msg, Seq},
        timer:sleep(random:uniform(Jitter))
    end, Agreed).

increment(Next, Seq) ->
    increment(maxSeq(Next, Seq)).

agreed(_, Queue) ->
    lists:splitwith(fun(X) ->
        case X of
            { agreed, _, _, _ } -> true;
            _                   -> false
        end
    end, Queue).
    

update(Queue, Ref, Seq) ->
    case lists:keyfind(Ref, 2, Queue) of
        { _, Ref, Msg, _ } -> 
            orderByNext(lists:keyreplace(Ref, 2, Queue, { agreed, Ref, Msg, Seq }))
    end.

agree(Nodes,Ref,Seq) ->
    lists:map(fun(M) -> M ! {agreed, Ref, Seq} end, Nodes).

proposal(Proposal, Cast, Ref) ->
    {Ref2, N, Sofar} = lists:keyfind(Ref, 1, Cast),
    if (N - 1) == 0 -> {agreed, maxSeq(Proposal, Sofar), updateCastWith(Cast, {Ref2, N, Sofar}, Proposal)};
    true -> updateCastWith(Cast, {Ref2, N, Sofar}, Proposal)
    end.


updateCastWith(Cast, {RefFound, N, _}, Proposal) ->
    lists:map(fun({Ref, L, Sofar}) ->
                      if Ref == RefFound ->
                             {Ref, N-1, maxSeq(Proposal, Sofar)};
                      true ->
                             {Ref, L, Sofar}
                      end
              end, Cast).


maxSeq({N1, Id1}, {N2, Id2}) ->
    if N1 > N2 -> {N1, Id1};
    true -> {N2, Id2}
    end.

insert(Queue, Ref, Msg, Seq) ->
    orderByNext(Queue ++ [{proposal, Ref, Msg, Seq}]).

increment({N, Id}) ->
    {N + 1, Id}.

orderByNext(Queue) ->
    lists:sort(fun({_, _, _, {N1, _}}, {_, _, _, {N2, _}}) -> N1 < N2 end, Queue).

cast(Cast, NodesAmount, Ref) ->
    [{Ref, NodesAmount, {0, self()}}|Cast].

request(Nodes, From, Ref, Msg) ->
    lists:map(fun(M) -> M ! {request, From, Ref, Msg} end, Nodes).