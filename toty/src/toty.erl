-module(toty).
-export([start/4, start1/2, start2/2, stop/0]).

start1(Sleep, Jitter) ->
    start(multicast1, worker1, Sleep, Jitter).

start2(Sleep, Jitter) ->
    start(multicast2, worker2, Sleep, Jitter).

start(MulticastType, WorkerType, Sleep, Jitter) ->
    M1 = apply(MulticastType, start, [Jitter]),
    M2 = apply(MulticastType, start, [Jitter]),
    M3 = apply(MulticastType, start, [Jitter]),
    M4 = apply(MulticastType, start, [Jitter]),
    M1 ! {peers, [M2, M3, M4, M1]},
    M2 ! {peers, [M1, M3, M4, M2]},
    M3 ! {peers, [M2, M1, M4, M3]},
    M4 ! {peers, [M2, M3, M1, M4]},
    register(w1, apply(WorkerType, start, ["John", 37, Sleep, M1])),
    register(w2, apply(WorkerType, start, ["Paul", 42, Sleep, M2])),
    register(w3, apply(WorkerType, start, ["George", 39, Sleep, M3])),
    register(w4, apply(WorkerType, start, ["Ringo", 41, Sleep, M4])),
    ok.

stop() ->
    stop(w1), stop(w2), stop(w3), stop(w4).

stop(Name) ->
    case whereis(Name) of
        undefined ->
            ok;
        Pid ->
            Pid ! stop
    end.
