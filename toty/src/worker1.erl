-module(worker1).
-export([start/4]).

start(Name, Seed, Sleep, Multicast) ->
    spawn(fun() -> init(Name, Seed, Sleep, Multicast) end).

init(Name, Seed, Sleep, Multicast) ->
    Gui = spawn(gui, init, [Name]),
    random:seed(Seed, Seed, Seed),
    Multicast ! {join, self()},
    worker(Name, Sleep, Gui, Multicast, {0, 0, 0}),
    Gui ! stop.


% cada worker tiene su GUI, si esta funcionando bien todas las ventanitas
% deberian ser del mismo color
worker(Name, Sleep, Gui, Multicast, State) ->
    Wait = random:uniform(Sleep),
    receive
        stop ->
            ok
        after Wait ->
            send_multicast(Name, Sleep, Gui, Multicast, State)
    end.

update(State, N) ->
    {R, G, B} = State,
    {G, B, R+N rem 256}.

send_multicast(Name, Sleep, Gui, Multicast, State) ->
    Multicast ! {multicast, self(), random:uniform(20)},
    waiting(Name, Sleep, Gui, Multicast, State).

waiting(Name, Sleep, Gui, Multicast, State) ->
    receive
        stop ->
            ok;
        {deliver, Ref, N} when Ref == self() ->
            NewState = update(State, N),
            Gui ! {update, NewState},
            worker(Name, Sleep, Gui, Multicast, NewState);
        {deliver, _, N} ->
            NewState = update(State, N),
            Gui ! {update, NewState},
            waiting(Name, Sleep, Gui, Multicast, NewState)
    end.
