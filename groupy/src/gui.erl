-module(gui).

-export([start/1, init/1]).

-include_lib("wx/include/wx.hrl").

start(Name) ->
    spawn(gui, init, [Name]).

init(Name) ->
    Width = 200,
    Height = 200,
    Server = wx:new(), %Server will be the parent for the Frame
    Frame = wxFrame:new(Server, -1, Name, [{size,{Width, Height}}]),
    wxFrame:setBackgroundColour(Frame, {0, 0, 0}),
    wxFrame:show(Frame),
    loop(Frame).

loop(Frame)->
    receive
        {update, State} ->
            wxFrame:setBackgroundColour(Frame, State),
            wxFrame:refresh(Frame),
            loop(Frame);
        stop ->
            wxFrame:setBackgroundColour(Frame, {0, 0, 0});
        Error ->
            io:format("gui: strange message ~w ~n", [Error]),
            loop(Frame)
    end.
