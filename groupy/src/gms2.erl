-module(gms2).
-export([start/3]).


start(Id, Grp, CrashTime) ->
    receive
        {master, Worker} ->
            io:format("master: ~p~n", [Worker]),
            case Grp of
                [] ->
                    initLeader(Id, Worker, CrashTime);
                [First|_] -> 
                    init(Id, First, Worker, CrashTime)
            end
        end.

initLeader(Id, Master, CrashTime) ->
    leader(Id, Master, [], [Master], CrashTime).

init(Id, Grp, Master, CrashTime) ->
    Self = self(),
    Grp ! {join, Master, Self},
    receive
        {view, [Leader|Slaves], Group} ->
            Master ! {view, Group},
            erlang:monitor(process, Leader),
            slave(Id, Master, Leader, Slaves, Group, CrashTime)
    after 3000 ->
        Master ! {error, "no reply from leader"}
    end.

leader(Id, Master, Slaves, Group, CrashTime) ->
    receive
        {mcast, Msg} ->
            bcast(Id, {msg, Msg}, Slaves, CrashTime),
            Master ! {msg, Msg},
            leader(Id, Master, Slaves, Group, CrashTime);
        {join, Wrk, Peer} ->
            io:format("leader join, ~p: ~p~n", [Id, Peer]),
            Slaves2 = lists:append(Slaves, [Peer]),
            Group2 = lists:append(Group, [Wrk]),
            bcast(Id, {view, [self()|Slaves2], Group2}, Slaves2, CrashTime),
            Master ! {view, Group2},
            leader(Id, Master, Slaves2, Group2, CrashTime);
        stop -> Master ! stop;
        poison -> self() ! stop;
        ping ->
            io:format("I'm ~p and I'm the leader!~n", [Id]),
            leader(Id, Master, Slaves, Group, CrashTime)
    end.

bcast(Id, Msg, Nodes, CrashTime) ->
    lists:foreach(fun(Node) -> Node ! Msg, crash(Id, CrashTime) end, Nodes).

crash(Id, CrashTime) ->
    case CrashTime of 
        666 -> ok;
        _ ->
        case rand:uniform(CrashTime) of
            0 -> ok;
            CrashTime ->
                io:format("leader ~w: crash~n", [Id]),
                exit(no_luck);
            _ -> ok
        end
    end.


slave(Id, Master, Leader, Slaves, Group, CrashTime) ->
    receive
        {mcast, Msg} ->
            Leader ! {mcast, Msg},
            slave(Id, Master, Leader, Slaves, Group, CrashTime);
        {join, Wrk, Peer} ->
            Leader ! {join, Wrk, Peer},
            slave(Id, Master, Leader, Slaves, Group, CrashTime);
        {msg, Msg} ->
            Master ! {msg,Msg},
            slave(Id, Master, Leader, Slaves, Group, CrashTime);
        {'DOWN', _Ref, process, Leader, _Reason} ->
            io:format("I'm ~p and my leader died!~n", [Id]),
            election(Id, Master, Slaves, Group, CrashTime);
        {view, [Leader|Slaves2], Group2} ->
            Master ! {view, Group2},
            slave(Id, Master, Leader, Slaves2, Group2, CrashTime);
        poison ->
            Leader ! poison,
            slave(Id, Master, Leader, Slaves, Group, CrashTime);
        ping ->
            Leader ! ping,
            slave(Id, Master, Leader, Slaves, Group, CrashTime);
        stop ->
            Master ! stop,
            io:format("slave ~p stopped~n", [Id])
    end.

election(Id, Master, Slaves, [_|Group], CrashTime) ->
    Self = self(),
    case Slaves of
        [Self|Rest] ->
            io:format("Im the new Leader: ~p~n", [Id]),
            bcast(Id, {view, Slaves, Group}, Rest, CrashTime),
            Master ! {view, Group},
            leader(Id, Master, Rest, Group, CrashTime);
        [Leader|Rest] ->
            erlang:monitor(process, Leader),
            slave(Id, Master, Leader, Rest, Group, CrashTime)
    end.
