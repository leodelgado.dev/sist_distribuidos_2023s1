-module(gms1).
-export([start/2]).


start(Id, Grp) ->
    receive
        {master, Worker} ->
            io:format("master: ~p~n", [Worker]),
            case Grp of
                [] ->
                    init(Id, Worker);
                [First|_] -> 
                    init(Id, First, Worker)
            end
        end.

init(Id, Master) ->
    leader(Id, Master, [], [Master]).

init(Id, Grp, Master) ->
    Self = self(),
    Grp ! {join, Master, Self},
    receive
        {view, [Leader|Slaves], Group} ->
            Master ! {view, Group},
            slave(Id, Master, Leader, Slaves, Group)
    end.

leader(Id, Master, Slaves, Group) ->
    receive
        {mcast, Msg} ->
            %%io:format("leader mcast, ~p: ~p~n", [Id, Msg]),
            %%io:format("leader slaves: ~p~n", [Slaves]),
            bcast(Id, {msg, Msg}, Slaves),
            Master ! {msg, Msg},
            leader(Id, Master, Slaves, Group);
        {join, Wrk, Peer} ->
            io:format("leader join, ~p: ~p~n", [Id, Peer]),
            Slaves2 = lists:append(Slaves, [Peer]),
            Group2 = lists:append(Group, [Wrk]),
            bcast(Id, {view, [self()|Slaves2], Group2}, Slaves2),
            Master ! {view, Group2},
            leader(Id, Master, Slaves2, Group2);
        stop -> ok
    end.

bcast(Id, Msg, Slaves) ->
    lists:foreach(fun(P) -> P ! Msg end, Slaves).


slave(Id, Master, Leader, Slaves, Group) ->
    receive
        {mcast, Msg} ->
            Leader ! {mcast, Msg},
            slave(Id, Master, Leader, Slaves, Group);
        {join, Wrk, Peer} ->
            Leader ! {join, Wrk, Peer},
            slave(Id, Master, Leader, Slaves, Group);
        {msg, Msg} ->
            Master ! {msg,Msg},
            slave(Id, Master, Leader, Slaves, Group);
        {view, [Leader|Slaves2], Group2} ->
            Master ! {view, Group2},
            slave(Id, Master, Leader, Slaves2, Group2);
        stop ->
            ok
    end.
