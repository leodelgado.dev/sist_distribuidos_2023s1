-module(gms3).
-export([start/3]).

%% todos los msj tanto comun como de join al grupo ahora van identificados con un id N

start(Id, Grp, CrashTime) ->
    receive
        {master, Worker} ->
            case Grp of
                [] ->
                    initLeader(Id, Worker, CrashTime);
                [First|_] -> 
                    init(Id, First, Worker, CrashTime)
            end
        end.

initLeader(Id, Master, CrashTime) ->
    leader(Id, Master, [], [Master], CrashTime, time:inc(time:zero())).

init(Id, Grp, Master, CrashTime) ->
    Self = self(),
    Grp ! {join, Master, Self},
    receive
        {view, N, [Leader|Slaves], Group} ->
            Master ! {view, Group},
            erlang:monitor(process, Leader),
            slave(Id, Master, Leader, Slaves, Group, CrashTime, time:inc(N), N)
    after 3000 ->
        Master ! {error, "no reply from leader"}
    end.

%% N: el id int del proximo msg a enviar, cada que envio un msj de cualquier tipo debo incrementarlo
leader(Id, Master, Slaves, Group, CrashTime, N) ->
    receive
        {mcast, Msg} ->
            bcast(Id, {msg, N, Msg}, Slaves, CrashTime),
            Master ! {msg, Msg},
            leader(Id, Master, Slaves, Group, CrashTime, time:inc(N));
        {join, Wrk, Peer} ->
            io:format("Joined to leader ~p: ~p~n", [Id, Peer]),
            Slaves2 = lists:append(Slaves, [Peer]),
            Group2 = lists:append(Group, [Wrk]),
            bcast(Id, {view, N, [self()|Slaves2], Group2}, Slaves2, CrashTime),
            Master ! {view, Group2},
            leader(Id, Master, Slaves2, Group2, CrashTime, time:inc(N));
        stop -> Master ! stop;
        poison -> Master ! stop;
        ping ->
            io:format("I'm ~p and I'm the leader!~n", [Id]),
            leader(Id, Master, Slaves, Group, CrashTime, N)
    end.

bcast(Id, Msg, Nodes, CrashTime) ->
    lists:foreach(fun(Node) -> Node ! Msg, crash(Id, CrashTime) end, Nodes).

crash(Id, CrashTime) ->
    case CrashTime of 
        666 -> ok;
        _ ->
        case rand:uniform(CrashTime) of
            0 -> ok;
            CrashTime ->
                io:format("leader ~w: crash~n", [Id]),
                exit(no_luck);
            _ -> ok
        end
    end.

%% N: el id int del proximo msg que espero recibir
%% Last: el id int del ultimo msg recibido por el leader
slave(Id, Master, Leader, Slaves, Group, CrashTime, N, Last) ->
    receive
        {mcast, Msg} ->
            Leader ! {mcast, Msg},
            slave(Id, Master, Leader, Slaves, Group, CrashTime, N, Last);
        {join, Wrk, Peer} ->
            Leader ! {join, Wrk, Peer},
            slave(Id, Master, Leader, Slaves, Group, CrashTime, N, Last);
        {msg, I, Msg} when I =< N ->
            io:format("[~p] I'm ~p and I've received a msg: ~p~n", [I, Id, Msg]),
            Master ! {msg, Msg},
            slave(Id, Master, Leader, Slaves, Group, CrashTime, time:inc(I), I);
        {'DOWN', _Ref, process, Leader, _Reason} ->
            io:format("I'm ~p and my leader died!~n", [Id]),
            election(Id, Master, Slaves, Group, CrashTime, N, Last);
        {view, I, [Leader|Slaves2], Group2} when I =< N ->
            Master ! {view, Group2},
            slave(Id, Master, Leader, Slaves2, Group2, CrashTime, time:inc(I), I);
        poison ->
            Leader ! poison,
            slave(Id, Master, Leader, Slaves, Group, CrashTime, N, Last);
        ping ->
            Leader ! ping,
            slave(Id, Master, Leader, Slaves, Group, CrashTime, N, Last);
        stop ->
            Master ! stop,
            io:format("slave ~p stopped~n", [Id])
    end.

election(Id, Master, Slaves, [_|Group], CrashTime, N, Last) ->
    Self = self(),
    case Slaves of
        [Self|Rest] ->
            io:format("Im the new Leader: ~p~n", [Id]),
            bcast(Id, {view, Last, Slaves, Group}, Rest, CrashTime),
            Master ! {view, Group},
            leader(Id, Master, Rest, Group, CrashTime, N);
        [Leader|Rest] ->
            erlang:monitor(process, Leader),
            slave(Id, Master, Leader, Rest, Group, CrashTime, N, Last)
    end.
