-module(groupy).
-export([start/3, stopmany/1, start1/0, start2/0, add2/3, add3/3, start2safe/0, start3safe/0]).

start1() ->
    start(gms1, 3000, 100).

start2() ->
    start(gms2, 3000, 100).

start2safe() ->
    start(gms2, 3000, 666).

start3() ->
    start(gms3, 3000, 100).

start3safe() ->
    start(gms3, 3000, 666).

start(GmsModule, Jitter, CrashTime) ->
    Leader = apply(worker, start, [GmsModule, "John", Jitter, [], CrashTime]),
    Slave1 = apply(worker, start, [GmsModule, "Paul", Jitter, [Leader], CrashTime]),
    Slave2 = apply(worker, start, [GmsModule, "Ringo", Jitter, [Leader], CrashTime]),
    Slave3 = apply(worker, start, [GmsModule, "George", Jitter, [Leader], CrashTime]),
    register(l, Leader),
    register(f1, Slave1),
    register(f2, Slave2),
    register(f3, Slave3),
    ok.

add2(Id, Node, PID) ->
    Process = apply(worker, start, [gms2, Id, 3000, [Node], 666]),
    register(PID, Process).

add3(Id, Node, PID) ->
    Process = apply(worker, start, [gms3, Id, 3000, [Node], 666]),
    register(PID, Process).

stopmany(Processes) ->
    lists:foreach(fun(P) -> stop(P) end, Processes).

stop(Id) ->
    case whereis(Id) of
        undefined ->
            ok;
        Pid ->
            Pid ! stop
    end.
