-module(worker).
-export([start/5]).

start(GmsModule, Id, Jitter, Group, CrashTime) ->
    Gui = spawn(gui, init, [Id]),
    Multicast = spawn(GmsModule, start, [Id, Group, CrashTime]),
    spawn_link(fun()-> init(Id, Gui, Multicast, Jitter) end),
    Multicast.

init(Id, Gui, Multicast, Jitter) ->
    Multicast ! {master, self()},
    worker(Id, Gui, Multicast, Jitter).

worker(Id, Gui, Multicast, Jitter) ->
    Wait = rand:uniform(Jitter),
    receive
        {view, Group} ->
            worker(Id, Gui, Multicast, Jitter);
        {msg, Msg} ->
            Gui ! {update, Msg},
            worker(Id, Gui, Multicast, Jitter);
        stop -> 
            Gui ! stop,
            io:format("worker ~p stopped~n", [Id])
    after Wait ->
        send_multicast(Id, Gui, Multicast, Jitter)
    end.

send_multicast(Id, Gui, Multicast, Jitter) ->
    Multicast ! {mcast, get_random_msg()},
    worker(Id, Gui, Multicast, Jitter).

get_random_msg() ->
    R = rand:uniform(256),
    G = rand:uniform(256),
    B = rand:uniform(256),
    {R, G, B}.
