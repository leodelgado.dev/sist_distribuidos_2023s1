groupy
=====

# Gms2

Para facilitar el debug agregamos formas que nos permiten consultar quien es el lider y poder matarlo enviandole un mensaje a cualquier nodo del grupo, el cual se va a propagar hasta llegar al lider (en el caso de consulta enviamos el mensaje *ping*, en el caso de que querramos matarlo enviamos el mensaje *poison*).

**Algo que debemos prestar atención es que debemos hacer si, como esclavo, recibimos un mensaje de vista de un nuevo líder antes que hayamos detectado que el viejo líder murió. Deberíamos negarnos a manejar mensajes de vista a menos que hayamos visto un mensaje Down del líder? o simplemente deberíamos aceptar el mensaje del nuevo líder e ignorar el mensaje Down?**

Si nos negamos a manejar mensajes de vista hasta que llegue un mensaje Down podria pasar que es el primer mensaje de vista para unirse al grupo, ocasionando que la primera vez que lo reciba, un slave nunca se una. Podemos idear una solución en la que detecte que ignore el primer mensaje pero es mas simple aceptar el mensaje del nuevo lider e ignorar el mensaje Down ya que funcionalmente lo unico que va a ocurrir es que el slave vaya una vez por el flujo de elección, el cual no es necesario ya que ya se determinó un nuevo lider y no va a ser él.

# Gms3

Para representar ids de cada mensaje usamos el modulo de tiempos de lamport implementado en proyectos anteriores.

**Lo primero que debemos darnos cuentas es que Erlang garantiza el envío de mensajes. La especificación garantiza solo que los mensajes son entregados en orden FIFO, no que necesariamente lleguen. Nosotros construimos nuestro sistema basados en la entrega confiable de mensajes, algo que no está garantizado. Cómo deberíamos cambiar nuestra implementación para manejar la posibilidad de que se pierda un mensaje? Cuál sería el impacto de esto en la performance?**

Solucionar la perdida de mensajes al 100% requiere:
- Guardar todos los mensajes externamente.
- Recuperar el sistema entero al momento en el que ocurrio algo que ocasione la perdida de mensajes, por ejemplo volver al estado de envio de mensajes justo antes de que un lider muera.
Ambas cosas implican un impacto significativo a la performance ya que:
- Los mensajes pueden enviarse muy rapido y en gran cantidad por lo que guardarlos a todos es pesado
- Hay que descartar el estado entero de mensajes posteriores a que un lider muera, lo cual puede ser una gran cantidad de mensajes que descartemos pausando el sistema hasta que sea totalmente recuperado
