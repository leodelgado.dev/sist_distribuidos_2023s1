muty
=====

# Lock1

### ¿Sería posible usar la cola de mensajes Erlang y dejar que los mensajes se encolen hasta que se libere el lock? 

Si, pero queda menos explicito el uso del lock.

### La razón por la que lo implementamos de esta manera es para hacer explícito que los mensajes son tratados aún en el estado held. ¿Por qué no estamos esperando mensajes ok?

No esperamos mensajes OK ya que el proceso que entro en estado held ya posee el lock. Sí lo hacemos para los procesos en espera para saber cuando un worker dejo de estar en la seccion critica.

### ¿Funciona bien? ¿Qué ocurre cuando incrementamos el riesgo de un conflicto de lock? ¿Por qué?
* Los workers funcionan bien con los parametros de Sleep y Work, sin embargo cuando mas alto sean hay mas probabilidad de que ocurran deadlocks entre workers que en la primera version del lock se resuelve permitiendo que los mismos timeouteen si pasa una N cantidad de tiempo que puede alcanzarse sumando el tiempo que tardan los distintos workers en intentar acceder a la seccion critica (Sleep) y el tiempo que demoran en trabajar dentro de la seccion critica (Work).

# Lock2
* Para implementarlo utilizamos un criterio de prioridad en base al Id correspondiente de los distintos workers (a mayor Id, mayor prioridad) y propagamos el parámetro de Id desde el proceso de entrada hasta donde sea necesario. Al momento de recibir una request evaluamos la prioridad del worker que recibio en la request con su propia prioridad, en el caso de que el que llegó en la request sea menos prioritario,se guarda como Ref en la lista de Waiting, caso contrario se envia un mensaje OK al emisor y se crea una nueva Ref en el momento para guardarla en la lista de Refs.

### ¿Podemos garantizar que tenemos un solo proceso en la sección crítica en todo momento?
* En las primeras versiones que implementamos lock2 nos ocurria que entraba más de uno a la sección critica a la vez. Esto fue resuelto cuando descubrimos que era un problema relacionado con respecto a cómo se guardan las Refs en cada lock, haciendo una nueva request al lock que tenga más prioridad en caso de que lo hubiere.

###  ¿Qué tan eficiente es y cuál es la desventaja?
* La desventaja es que al tener procesos ordenados por prioridad, los procesos con menos prioridad corren mayor riesgo de quedar en livelock mientras que los que tengan mayor prioridad logran entrar muchas más veces que el resto.
* A causa de esta desventaja, algunos procesos son más y otros menos eficientes a comparación de las otras implementaciones de lock.

# Lock3
* Para implementarlo creamos un modulo que gestiona los tiempos de Lamport basandonos en el TP de Loggy. Al enviar una solicitud, el reloj de Lamport del lock se incrementa cuando el worker obtiene el lock y se actualiza cuando recibis una request, quedandose con el tiempo mayor. En caso de que al comparar los tiempos sean similares, desempatamos por prioridad segun Ids que implementamos para el Lock2.

### ¿Puede darse la situación en que un worker no se le da prioridad al lock a pesar de que envió el request a su lock con un tiempo lógico anterior al worker que lo consiguió?
* Es posible si otro lock con tiempo mayor obtiene primero el lock, mientras que el anterior no llegue a consultar a todos los que deba a tiempo.
* Notamos una mejora considerable con respecto a lock2 ya que los accesos a la sección crítica son mucho más equitativos entre workers.