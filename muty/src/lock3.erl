-module(lock3).

-export([start/1]).

start(Id) ->
    spawn(fun() -> init(Id) end).

init(Id) ->
    receive
        {peers, Peers} ->
            open(Peers, Id, time:zero());
        stop ->
            ok
    end.

open(Nodes, Id, Time) ->
    receive
        {take, Master} ->
            Refs = requests(Nodes, Id, time:inc(Time)),
            wait(Nodes, Master, Refs, [], Id, Time);
        {request, From, Ref, _, RTime} ->
            From ! {ok, Ref},
            open(Nodes, Id, time:merge(Time, RTime));
        stop ->
            ok
    end.

requests(Nodes, Id, Time) ->
    lists:map(fun(P) ->
            R = make_ref(), 
            P ! {request, self(), R, Id, Time},
            R
    end, Nodes).

wait(Nodes, Master, [], Waiting, Id, Time) ->
    Master ! taken,
    held(Nodes, Waiting, Id, Time);

wait(Nodes, Master, Refs, Waiting, Id, Time) ->
    receive
        {request, From, Ref, FromId, FromTime} when Time == FromTime ->
            if FromId > Id ->
                R = make_ref(),
                From ! {ok, Ref},
                From ! {request, self(), R, Id, Time},
                wait(Nodes, Master, [R | Refs], Waiting, Id, Time);
            true ->
                wait(Nodes, Master, Refs, [{From, Ref}|Waiting], Id, Time)
            end;
        {request, From, Ref, _, FromTime} ->
            case time:leq(Time, FromTime) of
                true ->
                    wait(Nodes, Master, Refs, [{From, Ref} | Waiting], Id, time:merge(Time, FromTime));
                false ->
                    From ! {ok, Ref},
                    wait(Nodes, Master, Refs, Waiting, Id, time:merge(Time, FromTime))
            end;
        {ok, Ref} ->
            Refs2 = lists:delete(Ref, Refs),
            wait(Nodes, Master, Refs2, Waiting, Id, Time);
        release ->
            ok(Waiting),
            open(Nodes, Id, Time)
    end.

ok(Waiting) ->
    lists:foreach(fun({F,R}) -> F ! {ok, R} end,  Waiting).

held(Nodes, Waiting, Id, Time) ->
    receive
        {request, From, Ref, _, FromTime} ->
            held(Nodes, [{From, Ref}|Waiting], Id, time:merge(FromTime, Time));
        release ->
            ok(Waiting),
            open(Nodes, Id, Time)
    end.
