# Opty

- ¿Performa?

Respecto al rendimiento, la ejecución de cada instrucción es rápida, aunque el Commit tarda un poco más ya que se debe validar el commit y el validador es único. Se realizó una prueba de benchmark que recibe un número de cliente y operaciones que se desea que el cliente realice. Se generan concurrentemente la cantidad de clientes pedidos y estos ejecutan las operaciones alternando entre la elección aleatoria de leer o escribir. Al finalizar, realizan un commit e informan el resultado al proceso principal para que recolecte la información de los clientes.

- Pruebas realizadas:

```
100 clientes, 1 operaciones cada uno, 100 entradas
  Total clients: 100
  Total entries: 100
###########################################
  Total reads: 54
  Total writes: 46
  Total commits: 100
  Total commits fails: 11
###########################################
  Commits failure rate: 0.11
###########################################
  Total test time: 0.00132
```

```
100 clientes, 5 operaciones cada uno, 100 entradas
  Total clients: 100
  Total entries: 100
###########################################
  Total reads: 246
  Total writes: 254
  Total commits: 100
  Total commits fails: 62
###########################################
  Commits failure rate: 0.62
###########################################
  Total test time: 0.004061
```

```
100 clientes, 5 operaciones cada uno, 1000 entradas
  Total clients: 10
  Total entries: 1000
###########################################
  Total reads: 255
  Total writes: 245
  Total commits: 100
  Total commits fails: 18
###########################################
  Commits failure rate: 0.18
###########################################
  Total test time: 0.003837
```

```
10 clientes, 2 operaciones cada uno, 2 entradas
Total clients: 10
  Total entries: 2
###########################################
  Total reads: 11
  Total writes: 9
  Total commits: 10
  Total commits fails: 7
###########################################
  Commits failure rate: 0.7
###########################################
  Total test time: 0.000248
```

- ¿Cuáles son las limitaciones en el número de transacciones concurrentes y la tasa de éxito? ¿Algo más?

Las pruebas de benchmark arrojaron diferentes resultados. Se notó que la tasa de éxito por commit se mide de forma proporcionalmente indirecta con la cantidad de clientes multiplicada por la cantidad de operaciones que realizan (cantidad de transacciones) y la cantidad de entradas (tamaño del store). Mientras más grande sea el primer número y menor el segundo, aumentará la tasa de fallos. Mientras más chico sea el primer número y mayor el segundo, aumentará la tasa de éxito.

La limitación del número de transacciones concurrentes y la tasa de éxito depende de la cantidad de clientes y operaciones que se deseen realizar y el tamaño del store. En el store no se soportan muchos registros, y el máximo que puede soportar depende de la memoria del equipo y ronda los 100,000.

- ¿Es realista la implementación del store que tenemos? 

La implementación del store no es del todo realista, ya que el validador es un cuello de botella al ser un único proceso validando los commits de todos los usuarios. Mientras más clientes hacen commit, más tarda en responderle a cada uno. Además, el handler corre en el cliente y se le pasan el PID del servidor y el validador, lo que dificulta la distribución del trabajo. Se deberían crear varios servidores con sus respectivos validadores, los cuales posean una copia del store para operar de forma más eficiente. Un pro es que el proceso del handler muere si el cliente muere, pero también puede abrir un número descontrolado de handlers o que un mismo cliente abra todos los handlers que desee.

- Independientemente del handler de transacciones, ¿qué rápido podemos operar sobre el store?

Es rápido porque no depende de otro módulo, solo cuando se le solicita añade o toma de la lista.

- ¿Qué sucede si hacemos esto en una red de Erlang distribuida, qué es lo que se copia cuando el handler de transacciones arranca? 

En una red de Erlang distribuida, cuando el handler de transacciones arranca, se copia el cliente, el validador y el store. 

-¿Dónde corre el handler? 

El handler se genera en un proceso nuevo del lado del cliente.

-¿Cuáles son los pros y contras de la estrategia de implementación?

En cuanto a los pros y contras de la estrategia de implementación, el uso del handler ayuda a realizar todas las operaciones de lectura y escritura, y el store que almacena los datos hace las escrituras directamente al archivo y toma un dato en caso de necesitarlo. No obstante, el validador es un cuello de botella, lo que dificulta la distribución del trabajo, y el número de handlers que se pueden abrir no está controlado.




