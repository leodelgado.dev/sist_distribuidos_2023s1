-module(server).
-export([start/1, stop/0]).

start(N) ->
    Server = spawn(fun() -> init(N) end),
    register(server, Server).

init(N) ->
    Store = store:new(N),
    Validator = validator:start(),
    server(Validator, Store).

server(Validator, Store) ->
    receive
        {open, Client} ->
            Client ! {transaction, Validator, Store},
            server(Validator, Store);
        stop ->
            store:stop(Store)
    end.

stop() ->
    server ! stop.
