%%%-------------------------------------------------------------------
%% @doc opty public API
%% @end
%%%-------------------------------------------------------------------

-module(opty_app).

-behaviour(application).

-export([start/2, stop/1]).

start(_StartType, _StartArgs) ->
    opty_sup:start_link().

stop(_State) ->
    ok.

%% internal functions
